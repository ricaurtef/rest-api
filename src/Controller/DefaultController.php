<?php

namespace App\Controller;


use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use GuzzleHttp\Client;
use GuzzleHttp\EntityBody;

class DefaultController extends Controller
{
    /**
     * @Route("/default", name="default")
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/DefaultController.php',
        ]);
    }


    /**
     * @param Request $request
     * @Post("api/Example")
     * @return static
     */
    public function postExampleAction(Request $request)
    {

        $client = new \GuzzleHttp\Client();
        $content = [
            "test"     => true,
            "language" => "es",
            "command"  => "PING",
            "merchant" => [
                "apiLogin" => 'MxuvK4GdmMwE9l5',
                "apiKey"   => 'ZHpOj58OqcTDuIp6fd3km762Yf'
            ]
        ];

        $request = $client->post('https://api.payulatam.com/reports-api/4.0/service.cgi',
            ['headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],  'json' => $content ]
        );

        $response = json_decode($request->getBody()->getContents(),true);

        return View::create(array('response' => $response),Response::HTTP_CREATED,[]);

    }
}
